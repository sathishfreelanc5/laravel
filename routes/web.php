<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/about',function(){
   return view('about');
});

Route::get('/contact',function(){
   return view('contact');
});

Route::get('test-page/{id}/{type?}',function($id,$type){
       if($id==1 && $type=="page")
       {
           return "This is First Page";
       }else if($id==1 && $type=="number"){
           return "This is First Number";
       }else if($id==1 && $type==null){
           return "This is First Something";
       }
})->name('page');

Route::get('sample-page',function(){
     return view('page');
}); //->name('page')
Route::get('/user_form',function(){
   $title="View Data Example";
   $title2="Easy To Pass Data in the view";
    return view('user_form',['title'=>$title,'title2'=>$title2]);
//    return view('user_form',['title'=>$title,'title2'=>$title2]);
//    return view('user_form',compact('title','title2'));
});

Route::post('/get-userdata',function(Request $request){
      $name=$request->input('name');
      $age=$request->input('age');
      $phone=$request->input('phone');

      //Redirect To Page
      return redirect('user_form')->with('message','Successfully form Submitted !');
    //   return 'Hi,Your name is' .$name. 'and your age is' .$age. 'and your phone is'.$phone;
});

//Group The Route
Route::prefix('gallery')->group(function(){
    Route::get('photos',function(){
        return '<center><h2>Photos Page</h2></center>';
   });

    Route::get('videos',function(){
        return '<center><h2>videos Page</h2></center>';
    });

    Route::get('Documents',function(){
        return '<center><h2>Document Page</h2></center>';
    });
});

//Middleware

Route::get('month/{num}',function($num){
       if($num==1){
          return 'JANUARY';
       }else if($num==2){
         return 'FEBRUARY';
       }else if($num==3){
         return 'MARCH';
       }
})->middleware('month');


